<?php


namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class MyTestMiddleware
{
    public function handle($request,$controller)
    {

        $v = Validator::make($request->all(),
            [
                'a' => 'numeric',
                'b' => 'numeric',
                'operator' => 'in:sum,minus,mul,div',
            ]
        );

        if ($v->fails()){
            Log::driver('calc')->info(
                'calculation validation failed',
                $v->errors()->getMessages()
                );
            throw new ValidationException($v);
        }

        Log::driver('calc')->info(
            'calculation validation pass',
            $request->all()
            );

        $response = $controller($request);

        return $response;
    }
}
