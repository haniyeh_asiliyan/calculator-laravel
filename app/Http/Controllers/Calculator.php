<?php


namespace App\Http\Controllers;


class Calculator extends Controller
{

    public function addAction()
    {

//        if (!auth()->check()){
//            redirect()->to('/login')-> throwResponse();
//        }


        $op = request('operator','sum');
        $a = request('a', 0);
        $b = request('b', 0);

        $data = [
            'a' => $a,
            'b' => $b,
        ];

        if ($op == 'sum') {
            $data['c'] = $a + $b;
            $data['sign'] = '+';
        }elseif ($op == 'minus') {
            $data['c'] = $a - $b;
            $data['sign'] = '-';
        }elseif ($op == 'mul'){
            $data['c'] = $a * $b;
            $data['sign'] = '*';
        }elseif ($op == 'div'){
            $data['c'] = $a / $b;
            $data['sign'] = '/';
        }

        return view('calculate', $data);
    }
}

