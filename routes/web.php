<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Calculator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','welcome');

Route::middleware(['auth', 'myMD'])
    ->group(function (){
        Route::get('/calculate', [Calculator::class, 'addAction'])
            ->name('calc.add');
    });

//Route::get('/add/{a?}/{b?}', [Calculator::class, 'add'])
//    ->where($constraint);

//Route::get('/minus/{a?}/{b?}', [Calculator::class, 'minus'])
//    ->where($constraint);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
